# Testing

Of-note ; the tests are "boiled" or "burned" before being checked.

* **boiled** - `([ \t\r]*\n)+` is replaced with `\n`
* **burned** - `([ \t\r]*(//[^\n]*)?\n)+` is replaced with `\n`

Neither case really deals with `nameify()`
