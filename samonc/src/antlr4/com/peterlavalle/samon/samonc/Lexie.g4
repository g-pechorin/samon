grammar Lexie;

CR : '\r' ->channel(HIDDEN);

WS : ('\t'|' ');
NL : '\n';

ws : WS|NL;

SBO : '[';
SBC : ']';
EQ : '=';
COMMA : ',';
PAR_O : '(';
PAR_C : ')';
COLON : ':';

MATCH : '$match';
CASE : '$case';
FOR : '$for';
IN : '$in';
EACH : '$each';
END : '$end';

REMOVED : '$val';

BLOCK_O : '${';
BLOCK_C : '}$';
VALUE_O : '$(';
VALUE_C : ')$';

TYPE : (LETTER ALPHA_NUMERIC* '.')+ ALPHA_NUMERIC+;

NAME : LETTER ALPHA_NUMERIC* ;


module : (ws|using|block)* prototype WS* chunk* ;

chunk
	: block #embed_block
	| raw #emit_raw
	| emit #emit_value
	| MATCH raw casein+ END #match_case
	;

casein : CASE raw IN chunk+;

block : BLOCK_O raw BLOCK_C;
emit : VALUE_O raw VALUE_C;

arg : ws* NAME explicit_type ws* ;

explicit_type : ws* COLON class_name;

prototype : '$template(' arg (COMMA arg)* PAR_C;

using : '$import' ws class_name;



raw : (EQ|RAW|TYPE|COLON|PAR_O|PAR_C|NAME|COMMA|'$'|ws|SBO|SBC)+;

class_name : (NAME | TYPE | (ws+ class_name ws*)) (ws* SBO class_name (COMMA class_name)* SBC)?;

fragment
ALPHA_NUMERIC	: [a-zA-Z_0-9];

fragment
LETTER			: [a-zA-Z];

RAW : (~('$') | '$$');