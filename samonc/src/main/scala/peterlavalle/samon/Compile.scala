package peterlavalle.samon

import com.peterlavalle.samon.samonc.LexieParser

import scala.collection.JavaConversions._




object Compile {
	def block(context: LexieParser.BlockContext) =
		Roe.Embed(
			context.raw().getText,

			context.getStart.getTokenSource.getSourceName,

			context.getStart.getLine, context.getStart.getCharPositionInLine
		)

	def chunks(src: List[LexieParser.ChunkContext]) = {

		def recur(todo: List[LexieParser.ChunkContext], done: List[Roe.TChunk]): List[Roe.TChunk] =
			todo match {
				case Nil => done.reverse
				case head :: tail =>
					val next =
						head match {
							case blockContext: LexieParser.Embed_blockContext =>
								block(blockContext.block())

							case matchContext: LexieParser.Match_caseContext =>
								Roe.Match(
									matchContext.raw().getText.trim,

									matchContext.casein().toList.map {
										case context: LexieParser.CaseinContext =>
											context.raw().getText.trim -> recur(context.chunk().toList, List())
									},

									matchContext.getStart.getTokenSource.getSourceName,

									matchContext.getStart.getLine, matchContext.getStart.getCharPositionInLine
								)

							case rawContext: LexieParser.Emit_rawContext =>
								Roe.Raw(
									// don't need to escape bytes!
									rawContext.getText.replaceAll("\r?\n", "\n"),

									rawContext.getStart.getTokenSource.getSourceName,

									rawContext.getStart.getLine, rawContext.getStart.getCharPositionInLine
								)

							case valueContext: LexieParser.Emit_valueContext =>
								Roe.Emit(
									valueContext.emit().raw().getText.trim,

									valueContext.getStart.getTokenSource.getSourceName,

									valueContext.getStart.getLine, valueContext.getStart.getCharPositionInLine
								)
						}

					recur(tail, next :: done)
			}

		recur(src, List())
	}
}
