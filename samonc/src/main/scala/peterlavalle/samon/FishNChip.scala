package peterlavalle.samon

import java.io.{InputStream, StringWriter, Writer}
import java.util

import com.peterlavalle.samon.samonc.{LexieLexer, LexieParser}
import org.antlr.v4.runtime._
import org.antlr.v4.runtime.atn.ATNConfigSet
import org.antlr.v4.runtime.dfa.DFA

object FishNChip {
	def apply(name: String, inputStream: InputStream): String =
		apply(name, inputStream, new StringWriter()).toString

	def apply[W <: Writer](name: String, inputStream: InputStream, writer: W): W = {
		object ErrorThrowingListener extends ANTLRErrorListener {
			override def reportContextSensitivity(recognizer: Parser, dfa: DFA, startIndex: Int, stopIndex: Int, prediction: Int, configs: ATNConfigSet): Unit = {}

			override def reportAmbiguity(recognizer: Parser, dfa: DFA, startIndex: Int, stopIndex: Int, exact: Boolean, ambigAlts: util.BitSet, configs: ATNConfigSet): Unit = {}

			override def reportAttemptingFullContext(recognizer: Parser, dfa: DFA, startIndex: Int, stopIndex: Int, conflictingAlts: util.BitSet, configs: ATNConfigSet): Unit = {}

			override def syntaxError(recognizer: Recognizer[_, _], offendingSymbol: scala.Any, line: Int, charPositionInLine: Int, message: String, e: RecognitionException): Unit =
				sys.error("[samonc error] %s:%d ; %s".format(name, line, message))

		}
		Hatcher(
			writer,
			name, {
				val parser = new LexieParser(
					new CommonTokenStream({
						val lexer = new LexieLexer({
							val antlrStream: ANTLRInputStream = new ANTLRInputStream(inputStream)
							antlrStream.name = name
							antlrStream
						})

						lexer.removeErrorListeners()
						lexer.addErrorListener(ErrorThrowingListener)

						lexer
					})
				)

				parser.removeErrorListeners()
				parser.addErrorListener(ErrorThrowingListener)

				parser
			}.module()
		)
	}
}
