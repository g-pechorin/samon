package peterlavalle.samon

import java.io.Writer

import com.peterlavalle.samon.samonc.LexieParser
import peterlavalle.samon.Roe.Raw
import peterlavalle.samon.runtime.Egg

import scala.collection.JavaConversions._

// It would be super-sweet if we could bootstrap the build ... wait ... okay ; maybe next time. for now ; here's how the templates are created
object Hatcher {

	val sNameRegex: String = "(.*)/(\\w+)$"

	def apply[W <: Writer](writer: W, name: String, module: LexieParser.ModuleContext): W = {
		require(name.matches("(\\w+/)*\\w+"))

		// declare the package
		if (name.contains("/")) {
			writer.append("package %s\n\n".format(name.replaceAll(sNameRegex, "$1").replace("/", ".")))
		}

		// scan imports
		imports(module).foreach(n => writer.append("import %s\n".format(n)))

		writer.append("\nobject %s {\n".format(name.replaceAll(sNameRegex, "$2")))

		def writeLocation(egg: Egg, prefix: String, chunk: Roe.TChunk) =
			egg.println(
				"%s// %s @ %s/%s".format(prefix, chunk.file, chunk.line, chunk.pos)
			)

		// do any preamble stuff
		module.block().toList.map(Compile.block).foldLeft(new Egg()) {
			case (egg, block) =>
				writeLocation(egg.append("\n"), "\t", block)
					.println(block.content)

		}(writer)

		// decode chunks
		// knit together raw chunks
		val knited: List[Roe.TChunk] =
			Roe.knit(
				Compile.chunks(
					module.chunk().toList
				)
			)


		// visit all raw-chunk roe
		val rawChunks: Map[String, String] = {

			// the relative complexity of this approach reuses identical blocks of output

			val allRaw: List[(Roe.Raw, Int)] =
				Roe.visit(
					List[Roe.Raw](),
					knited,
					(tail: List[Roe.Raw], n: Roe.TChunk) =>
						n match {
							case head: Roe.Raw =>
								head :: tail
							case _ =>
								tail
						}).reverse.zipWithIndex


			// write raw chunks as pre-chopped character arrays (... because escaping those strings is boooring)
			allRaw.foldLeft(new Egg()) {
				case (egg, (raw: Roe.Raw, index)) =>

					val filter: List[(Raw, Int)] = allRaw.filter(_._1.content == raw.content)

					val p = filter.foldLeft(egg)((e, r) => writeLocation(e, "\n\t", r._1))

					if (filter.head._2 == index) {
						p.println(
							"\tprivate final val charBlock%s: Array[Char] = Array(%s)"
								.format(
									index, raw.content.toCharArray.map(_.toInt + ".toChar, ").foldLeft("")(_ + _).replaceAll(", $", "")
								)
						)
					} else {
						p
					}
			}.apply(writer)

			// map the chunk's content to the written pre-chopped array
			allRaw.foldLeft(Map[String, String]()) {
				case (map, (r, i)) =>
					val c: String = r.content
					if (map.contains(c))
						map
					else {
						map ++ Map(r.content -> "samonStream.append(charBlock%d)".format(i))
					}
			}
		}

		def nameify(prefix: String, anyRef: AnyRef*): String =
			anyRef.map(v => Integer.toHexString(Math.abs(v.hashCode))).foldLeft(prefix)(_ + _)


		// create the easy method then open the function with parameters
		val w = nameify("w", knited)
		writer.append("\n\t" + {

			val args: String = parameters(module).foldLeft("")(_ + ", " + _).replaceAll("^, ", "")
			val params: String = args.split(", ").map(_.replaceAll("\\:.*", ", ")).foldLeft("")(_ + _).replaceAll(", $", "")

			"""
				|	def apply({args}): String =
				|		apply(new java.io.StringWriter(), {params}).toString
				|
				|	def apply[W <: java.io.Writer]({w}: W, {args}): W = {
				|		val samonStream = new peterlavalle.samon.runtime.Egg()
			""".stripMargin.trim
				.replace("{args}", args)
				.replace("{params}", params)
				.replace("{w}", w)
		})

		/**
		 * Write a series of chunks to an egg
		 */
		def layEgg(prefix: String, chunks: List[Roe.TChunk], egg: Egg): Egg =
			chunks match {
				case Nil =>
					egg
				case head :: tail =>
					writeLocation(egg, "\n" + prefix, head)
					layEgg(prefix, tail, head match {

						case Roe.Embed(content: String, _, _, _) =>
							egg.println(prefix + content)

						case Roe.Emit(content: String, _, _, _) =>
							egg.println(prefix + "samonStream.append({%s})".format(content))

						case Roe.Match(content: String, cases: List[(String, List[Roe.TChunk])], _, _, _) =>
							cases.foldLeft(egg.println(prefix + "(%s).foreach {".format(content))) {
								case (e, (local, contents)) =>
									e.println(prefix + "\t\tcase " + local + " =>")
									layEgg(prefix + "\t\t\t", contents, e)
							}
								.println(prefix + "}")

						case Roe.Raw(content: String, _, _, _) if rawChunks.contains(content) =>
							egg.println(prefix + rawChunks(content))

						case Roe.Raw(content: String, _, _, _) =>
							System.err.println("TODO HACK FIXME (... there's ugliness in the generator - but the templates work fine)")
							System.err.println("Seriuosly tell Peter that this message came out - he might remove this panic route otherwise")
							egg.println(prefix + "samonStream.append(Array[Char](" + content.toCharArray.foldLeft("")(_ + ", " + _.toInt + ".toChar").replaceAll("^, ", "") + "))")

					})
			}

		// emit all chunks
		layEgg("\t\t", knited, new Egg().append("\n"))
			.apply(writer)

		// close the function / object
		writer.append(
			"""
				|		samonStream({w})
				|	}
				|}
			""".stripMargin
				.replace("{w}", w)
		).asInstanceOf[W]
	}

	def imports(module: LexieParser.ModuleContext): List[String] =
		module.using().toList.map(_.class_name().getText)

	def parameters(module: LexieParser.ModuleContext): List[String] =
		module.prototype().arg().toList.map(arg => "%s: %s".format(arg.NAME().getText, arg.explicit_type().class_name().getText.trim))

}
