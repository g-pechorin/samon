package peterlavalle.samon

/**
 * Get it? Roe is fish eggs. Roe is fish eggs Coral!
 */
object Roe {

	sealed trait TChunk {
		val file: String
		val line: Int
		val pos: Int
		val content: String
	}

	final def visit[T](input: T, todo: List[TChunk], visitor: (T, TChunk) => T): T = {
		todo match {
			case Nil =>
				input
			case head :: tail =>
				visit(
					visitor(input, head),
					head match {
						case is: Match =>
							is.cases.flatMap(_._2) ++ tail
						case _ =>
							tail
					},
					visitor
				)
		}
	}

	/**
	 * a chunk of embedded code
	 */
	case class Embed(content: String, file: String, line: Int, pos: Int) extends TChunk

	/**
	 * A statement that writes its own value to the output
	 */
	case class Emit(content: String, file: String, line: Int, pos: Int) extends TChunk

	/**
	 * A raw piece of text that should be passed as-is to the output
	 */
	case class Raw(content: String, file: String, line: Int, pos: Int) extends TChunk

	case class Match(content: String, cases: List[(String, List[TChunk])], file: String, line: Int, pos: Int) extends TChunk

	def knit(input: List[TChunk]): List[TChunk] = {
		def recur(todo: List[TChunk], done: List[TChunk]): List[TChunk] =
			todo match {
				case Nil =>
					done.reverse

				// strip empty Raw
				case Raw(null | "", _, _, _) :: tail =>
					recur(tail, done)

				// knit pairs of Raw
				case Raw(left, file, line, pos) :: Raw(right, _, _, _) :: tail =>
					recur(Raw(left + right, file, line, pos) :: tail, done)

				// strip empty Embed
				case Embed(null | "", _, _, _) :: tail =>
					recur(tail, done)

				// knit pairs of Embed
				case Embed(left, file, line, pos) :: Embed(right, _, _, _) :: tail =>
					recur(Embed(left + '\n' + right, file, line, pos) :: tail, done)

				case head :: tail =>
					recur(tail, head :: done)
			}

		recur(input, List())
	}
}
