package com.peterlavalle.samon.samonc

import java.io.InputStream

import junit.framework.Assert._
import junit.framework.TestCase
import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}
import peterlavalle.samon.c.TSamonTests
import peterlavalle.samon.{FishNChip, Hatcher}

import scala.io.Source

class HatcherTest extends TestCase with TSamonTests {

	def boil(source: String): String =
		source.trim.replaceAll("([ \t\r]*\n)+", "\n")

	def burn(source: String): String =
		source.trim.replaceAll("([ \t\r]*(//[^\n]*)?\n)+", "\n")

	override def apply(path: String): Unit = {

		val name: String = path.reverse.substring(6).reverse
		val resourceAsStream: InputStream = getClass.getClassLoader.getResourceAsStream(path)

		val boiled = getClass.getClassLoader.getResourceAsStream(name + ".scala.boiled")
		val burned = getClass.getClassLoader.getResourceAsStream(name + ".scala.burned")

		assertTrue(
			"Expectations for %s missing".format(path),
			boiled != null || burned != null
		)

		if (null != boiled)
			assertEquals(
				boil(Source.fromInputStream(boiled).mkString),
				boil(
					FishNChip(name, resourceAsStream)
				)
			)

		if (null != burned)
			assertEquals(
				burn(Source.fromInputStream(burned).mkString),
				burn(
					FishNChip(name, resourceAsStream)
				)
			)
	}

	def testParameters(): Unit =
		assertEquals(
			List("name: String", "f: Float", "i: Int", "b: Boolean"),
			Hatcher.parameters(
				compile(
					Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("Whole.samon")).mkString.replace("\r", "")
				)
			)
		)

	def testImports(): Unit =
		assertEquals(
			List("java.lang.String", "java.lang.String", "java.lang.RunTime", "java.lang.RunTime", "java.String"),
			Hatcher.imports(
				compile(
					Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("Whole.samon")).mkString.replace("\r", "")
				)
			)
		)

	def compile(source: String) = {


		val stream: ANTLRInputStream = new ANTLRInputStream(source)
		stream.name = getName

		new LexieParser(new CommonTokenStream(new LexieLexer(stream))).module()

	}
}
