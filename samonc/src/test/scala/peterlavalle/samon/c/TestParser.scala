package peterlavalle.samon.c

import com.peterlavalle.samon.samonc.{LexieLexer, LexieParser}
import junit.framework.TestCase
import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}

class TestParser extends TestCase with TSamonTests {

	def parser(path: String) = {
		new LexieParser(
			new CommonTokenStream(
				new LexieLexer({
					val stream: ANTLRInputStream = new ANTLRInputStream(
						ClassLoader.getSystemResourceAsStream(path)
					)
					stream.name = path
					stream
				})
			)
		)
	}

	override def apply(path: String): Unit = {
		parser(path).module()
	}
}
