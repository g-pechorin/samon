package peterlavalle.samon.runtime

import java.io.Writer
import java.util

import scala.collection.JavaConversions._

/**
 * Supports delayed writing of data
 */
class Egg {

	private val list = new util.LinkedList[TShell]()

	sealed trait TShell

	case class Hard(obj: Any) extends TShell

	case class Soft(egg: Egg) extends TShell

	case class Chunk(arr: Array[Char]) extends TShell

	def append(obj: Any) = {
		list.addLast(
			Hard(
				obj match {
					case arr: Array[Char] => new String(arr)
					case _ => obj
				}
			)
		)

		this
	}

	def println(obj: Any): Egg =
		append(obj).append(Array('\n'))


	def lay(): Egg = {
		val egg = new Egg()
		list.addLast(Soft(egg))
		egg
	}

	private val hooks = new util.HashMap[String, Any]()

	def update(key: String, value: Any): Unit = {
		hooks(key) = value
	}

	def apply[W <: Writer](w: W): W = {
		def recu(w: W, list: List[TShell], hooks: Map[String, Any]): W =
			list match {
				case Nil => w
				case head :: tail =>
					val n: W =
						head match {
							case Hard(obj) =>
								w.append(hooks.toList.foldLeft(obj.toString) {
									case (prev, (key, ref)) =>
										prev.replace("{" + key + "}", ref.toString)
								}).asInstanceOf[W]
							case Soft(egg) =>
								val toList: List[TShell] = egg.list.toList.map(_.asInstanceOf[TShell])
								recu(w, toList, hooks ++ egg.hooks)
						}

					recu(n, tail, hooks)
			}
		recu(w, list.toList, hooks.toMap)
	}

}
