Samon is a (static) text template engine - it consumes `.samon` files and produces `.scala` files.
(Static) Text Template Engines take a almost-plain-text file, chew it up, and spit it back out as a piece of source code that writes the original file back, but plugins in variables and loops and stuff.

* [Jamon](http://www.jamon.org/) is a static text template engine, written in Java, that I based my design on
* [Twirl](https://github.com/spray/twirl) is a static text template engine, written in Scala, which I assessed but didn't suit my needs
* [PHP](https://en.wikipedia.org/?title=PHP) is (more or less) a dynamic text template engine
* [Smarty](http://www.smarty.net/) is a static text template engine for PHP that I used years ago
 
Samon produces `.scala` files will have a Scala `object` made up of;

* a bunch of `char` arrays (because escaping strings is haaarrrrd)
* an overloaded apply method that
	* takes the parameters you specify
	* returns a `java.lang.String`
	* uses `java.io.StringWriter` to invoke the "real" apply method
* a "real" apply method which
	* starts with a generic `java.io.Writer`
	* takes the parameters you specify
	* returns a same-class generic `java.io.Writer`
	* implements the template you specified using your chunks and those `char` arrays

The remainder of this document should give a very brief explanation of how to use Samon's features.
Readers unfamiliar with (Scala) programming (or why you'd want to spew reams of text) might not get much out of this, but it should make some sense.

# [Basic Template](examples/usages/basic.samon)

You need to declare what the function's signature will be.

	$template(name: String)

This is all that you require for a template.
It won't write out anything interesting, but it will produce a valid file;

	package usages
	
    object basic {
    
    	def apply(name: String): String =
    		apply(new java.io.StringWriter(), name).toString
    		
    	def apply[W <: java.io.Writer](w1c395774: W, name: String): W = {
    		val samonStream = new peterlavalle.samon.runtime.Egg()
    		
    		samonStream(w1c395774)
    	}
    }
	
So;

* templates take arguments to create function-objects
* generated function-object optionally takes a generic `java.io.Writer` that it will write to
 	* the current iteration doesn't allow you direct access to the writer - but something similar
* generated function-object either returns a `java.lang.String` or the writer you passed in
 	* the current iteration collects all-the-things to write, then stringifies them to the writer at the end
* the writer's local name is randomised to prevent people from trying to mess with it
	* the system expects you to work with the egg instance
	* the egg instance supports `.append(...)` and `.println(...)` methods

... of course; this won't do anything interesting ... yet 

# [Embed Scala](examples/usages/ex01.samon)

Text-Template-Engines are cool because (in the middle of the blobs of text) you slip bits of logic into the template.
You can print a big-honking-file-header then do some calculations and append them!
A simpler example is to append a single object to the egg instance.

AFTER the `$template(...)` anything you plop between `${` and `}$` will be copied into your template's apply method.

	$template(name: String)
	
	Hello ${ samonStream.append(name) }$
	
Here we're passing our output to the `samonStream` which is a `java.io.Writer` like object that has `.append(...)` and `.println(...)` methods.
	
	package usages
	
	object ex01 {
	
		private final val charBlock0: Array[Char] = Array(10.toChar, 10.toChar, 9.toChar, 72.toChar, 101.toChar, 108.toChar, 108.toChar, 111.toChar, 32.toChar)
		
		def apply(name: String): String =
			apply(new java.io.StringWriter(), name).toString
		
		def apply[W <: java.io.Writer](w35b2b87c: W, name: String): W = {
			val samonStream = new peterlavalle.samon.runtime.Egg()
			samonStream.append(charBlock0)
			samonStream.append(name)
			samonStream(w35b2b87c)
		}
	}

(I didn't use a `java.io.Writer` directly because I wanted to deffer output)

# [Emit Values](examples/usages/ex02.samon)

Templates are supposed to write text.
So - there's a simpler way to write out a single value - even if that value is calculated from Scala's magic list methods

	$template(name: String)
	
	Hello $(name)$ - can I call you $(name.replaceAll("\\W", "").toUpperCase)$

The text that falls between the `$(` and the `)$` directives is plopped verbatim into a pair of `{` `}` and then written to the output.
So you can do fancy sequence mapping or whatnot to get a result.

# [Preamble Statements](examples/usages/ex03.samon)

You might want to add methods that can be used outside of your template (for testing or whatever)
Samon can do a preamble.
Anything surrounded by `${` and `}$` BEFORE the `$template(...)` symbol will be written to your template's body.

	${
		def identify(text: String) = text.replaceAll("\\W", "").toUpperCase
	}$
	
	$template(name: String)
	
	  Hello $(name)$ - are you $(identify(name))$

This produces the .scala;

	package usages
    
    object ex03 {
   
    def identify(text: String) = text.replaceAll("\\W", "").toUpperCase
    	
    	private final val charBlock0: Array[Char] = Array(10.toChar, 10.toChar, 32.toChar, 32.toChar, 72.toChar, 101.toChar, 108.toChar, 108.toChar, 111.toChar, 32.toChar)
    	private final val charBlock1: Array[Char] = Array(32.toChar, 45.toChar, 32.toChar, 97.toChar, 114.toChar, 101.toChar, 32.toChar, 121.toChar, 111.toChar, 117.toChar, 32.toChar)
    	private final val charBlock2: Array[Char] = Array(10.toChar)
    	
    	def apply(name: String): String =
    		apply(new java.io.StringWriter(), name).toString
    	
    	def apply[W <: java.io.Writer](w6d1e8cce: W, name: String): W = {
    		val samonStream = new peterlavalle.samon.runtime.Egg()
    		samonStream.append(charBlock0)
    		samonStream.append({name})
    		samonStream.append(charBlock1)
    		samonStream.append({identify(name)})
    		samonStream.append(charBlock2)
    		samonStream(w6d1e8cce)
    	}
    }
	
Not amazing but nicer (IYAM) when you have a bunch of case classes to match into small values.

(... i.e. when you have types that need to be translated into strings)

# Import Statements

You can import things too;
	
	$import peterlavalle.uniki.ir._
	
	$template(name: String)

The generated-source file now looks like this;

	package whatever.folder.you.used
	
	import peterlavalle.uniki.ir._
	
	object SStarServer {
	
		...
		chunks of text will be here
		...
	
		def apply(name: String): String =
			...

		def apply[W <: java.io.Writer](w448f99fd: W, name: String): W = {
			...
		}
	}

The engine tries to copy the import statements verbatim

# [Match Statements](examples/usages/ex06.samon)

Looping over some sort of sequence is useful.
Samon will construct a series of nested functions to handle that.
You can match almost like you're using Scala;
	
	${
		sealed trait TCase
	
		case class Foo(name: String) extends TCase
		case class Bar(number: Int) extends TCase
	}$
	
	$template(list: List[TCase])
	
	$match list.sortBy(_.toString)
	
	$case Foo(name: String) $in
		name -> $(name)$
		
	$case Bar(number) $in
		code -> $(number)$
		
	$end

This produces;

	package usages
	
    object ex06match {
    
    	sealed trait TCase
    	case class Foo(name: String) extends TCase
    	case class Bar(number: Int) extends TCase
    	
    	private final val charBlock0: Array[Char] = Array(10.toChar, 10.toChar)
    	private final val charBlock1: Array[Char] = Array(10.toChar, 9.toChar, 110.toChar, 97.toChar, 109.toChar, 101.toChar, 32.toChar, 45.toChar, 62.toChar, 32.toChar)
    	private final val charBlock2: Array[Char] = Array(10.toChar, 10.toChar)
    	private final val charBlock3: Array[Char] = Array(10.toChar, 9.toChar, 99.toChar, 111.toChar, 100.toChar, 101.toChar, 32.toChar, 45.toChar, 62.toChar, 32.toChar)
    	private final val charBlock4: Array[Char] = Array(10.toChar, 10.toChar)
    	private final val charBlock5: Array[Char] = Array(10.toChar, 9.toChar, 115.toChar, 104.toChar, 111.toChar, 101.toChar, 32.toChar, 58.toChar, 58.toChar, 32.toChar)
    	private final val charBlock6: Array[Char] = Array(10.toChar, 10.toChar)
    	
    	def apply(list: List[TCase]): String =
    		apply(new java.io.StringWriter(), list).toString
    	
    	def apply[W <: java.io.Writer](w3ecafac1: W, list: List[TCase]): W = {
    		val samonStream = new peterlavalle.samon.runtime.Egg()
    		
    		samonStream.append(charBlock6)
    		
    		( list.sortBy(_.toString)
    ).foreach {
    				case  Foo(name: String)  =>
    					samonStream.append(charBlock1)
    					samonStream.append({name})
    					samonStream.append(charBlock6)
    				case  Bar(number) if number > 2  =>
    					samonStream.append(charBlock3)
    					samonStream.append({number})
    					samonStream.append(charBlock6)
    				case  Bar(number: Int)  =>
    					samonStream.append(charBlock5)
    					samonStream.append({number})
    					samonStream.append(charBlock6)
    		}
    		samonStream(w3ecafac1)
    	}
    }

This is better for verbose matching situations
