Frustrated with [Jamon](http://www.jamon.org/)'s lack of Scala?
In a tizzy over [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates)'s lack of inline code?
Try Samon and see how fun templates can be!

# Samon

Samon was a hold-my-beer (well ... my coffee ...) static template engine.
It's inspired by / ripping off [Jamon](http://www.jamon.org/) and [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates).
I've been adjusting it whenver I feel that something is lacking or redundant.

In hindsight, Samon's design was meant to (mostly) allow Scala's `match` keyword with little-to-no other "stuffing" around it.
I did not find that Twirl was usable from-scratch for plain text files - so I rejected it.
Jamon uses `.java` as its language, so also wasn't suitable.
As time has passed I have wanted a more ... "[castrated](http://www.cs.usfca.edu/~parrt/papers/mvc.templates.pdf)" ... engine that'd be simpler to work with; but that's beyond my need/interest.

# Usage

## Style

Templates probably look pretiest when they're laid out as;

* imports
* arguments
* local vals
* content (and any vals that can' be init up-there)

## Gradle

You need to include it (and Scala) into your gradle project;

	apply plugin: 'samon'
	buildscript {
		repositories {
			// Samon uses stuff in central
			mavenCentral()
			mavenLocal()

			// Samon will be here
			maven {
				name "Peter's Stuff"
				url 'https://dl.dropboxusercontent.com/u/15094498/repo/snap'
			}
		}

		// Samon needs to be added as a dependency
		dependencies {
			classpath 'peterlavalle.samon:plugin:1.0.0.4'
		}
	}

With that in place, you can write templates in `src/samon` or whatever - you can configure this.

Like a good little tool - e adds eir dependencies et al (but not Scala itself - it should be mildly obvious why)

## Mojo / Maven

No more!

# Minutae

* runs at compile time (it's static like [Jamon](http://www.jamon.org/) and [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates))
* emits to plain old (Scala) source code (like [Jamon](http://www.jamon.org/) and [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates))
* lets you mess about with the stream writer (like [Jamon](http://www.jamon.org/), unlike [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates))
* embed random top-level blocks of code (like [Jamon](http://www.jamon.org/), unlike [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates))
* create locals (actually just an special case of the above) (like [Jamon](http://www.jamon.org/), sort of unlike [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates))
* lacks many features I don't need (but [Jamon](http://www.jamon.org/) and [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates) have these - I don't think I need it)
	* (you can DIY these yourself of course)
	* fancy way to invoke other templates
	* aging Eclipse plugins
	* conditionals
 	* if they're short use embeded scala code
 	* if they're long dial out to another template
	* specific escaping and encoding (for HTML or whatever)

I intend to use it to power the backend of a shader compiler.
Living with [Jamon](http://www.jamon.org/)'s not-Scala nature was a PITA.
Dealing with [Twirl](https://www.playframework.com/documentation/2.2.x/ScalaTemplates)'s no-val / no-writer / no-inline was as much of a PITA.

(NO - `@defining` is not a `val`)

(... though you could define a tuple of `val`s)

(... and it doesn't get around my desire to directly meddle with the output from outside the template)

(... and it doesn't let me embed chunks of scala in the template itself)

(... I know - I removed the `val` construct; it used the same number of chars as an inline chunk of code)

# Change Log

## 1.0.0.5 / 2015-07-01

* allows `$` in names (to support testing `.class` files) 

## 1.0.0.4 / 2015-06-15

* added match loops (there was a brief foreach, which was kind of redundant)
* added "preamble" option (stick global code before the template starts!) 
* dropped Maven support (and misc boilerplate classes)
* dropped `val` (change `$val eight { = 8 }$` to `${ val eight = 8 }$` and you'll have the same effect)
* gradle task automatically wires the stuff together
* uses a runtime to allow "deferred string creation"
* prevented duplicate blocks of chars
* rewrote a lot of the internals
* template format and gradle usage are basically-the-same (... actually - they're simpler)

## 0.0.0.2 / 2015-03-23

* changed gradle's expected source folder to `src/samon`

## 0.0.0.1 / 2015-03-20

Magically fixed a bug that I now can't find

* removed "SNAPSHOT" from the versions
* updated version to 0.0.0.1
* renamed the "grask" module "buildSrc"