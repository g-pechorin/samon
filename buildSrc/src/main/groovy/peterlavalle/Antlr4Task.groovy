package peterlavalle

import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.api.*;


/// ====
/// Big bad voodoo to run Antlr4
/// ----
class Antlr4Task extends DefaultTask {

	@InputDirectory
	def File grammarDir = project.file('src/antlr4');

	@OutputDirectory
	def File generatedDir = project.file("$project.buildDir/generated/antlr4-sources");

	{
		description = 'Translates Antlr4 .g4 grammars into .java sources'
		group = 'Source Code Generation'

		project.sourceSets.main.java.srcDirs += generatedDir
		project.compileJava.dependsOn(this)

		project.dependencies {
			compile 'org.antlr:antlr4-runtime:4.5'
		}
	}

	@TaskAction
	def generate() {
		assert (generatedDir.exists() || generatedDir.mkdirs())
		project.sourceSets.main.java.srcDirs += generatedDir

		project.fileTree(dir: grammarDir, include: ['**/*.g4']).files.sort().each {
			it ->
				def relative =
						it.absolutePath
								.substring(grammarDir.absolutePath.length() + 1, it.absolutePath.length() - 3)
								.replace('\\', '/')

				def pakcage = relative.substring(0, relative.lastIndexOf('/')).replace('/', '.')

				println("grammar ; $relative")
				def tool =
						new org.antlr.v4.Tool([
								"-o", new File(generatedDir, pakcage.replace('.', '/')).absolutePath,
								"-package", pakcage,
								"-no-listener",
								"-no-visitor",
								it.absolutePath
						] as String[])

				tool.processGrammarsOnCommandLine()

				if (tool.getNumErrors() > 0) {
					throw new GradleException("ANTLR 4 caught " + tool.getNumErrors() + " build errors.")
				}
		}
		println('grammars all processed')
	}
}
