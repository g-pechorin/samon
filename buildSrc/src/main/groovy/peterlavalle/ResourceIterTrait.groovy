package peterlavalle

import groovy.io.FileType
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.api.*;

class ResourceIterTrait extends DefaultTask {

	String generatedClass
	String testPattern

	File generatedDir = project.file('build/generated/resource-iter-trait');

	{
		project.sourceSets.test.scala.srcDirs += generatedDir
		project.compileTestScala.dependsOn(this)
	}

	@TaskAction
	def generate() {

		def traitFile = new File((File) generatedDir, (String) generatedClass.replace('.', '/') + '.scala')
		assert (traitFile.parentFile.exists() || traitFile.parentFile.mkdirs())
		def traitWriter = new FileWriter(traitFile)

		traitWriter.println('package ' + generatedClass.replaceAll('^(.*?)\\.?([^\\.]+)$', '$1'))
		traitWriter.println('')
		traitWriter.println("// magic trait to iterate over resources matching the pattern \"$testPattern\"")
		traitWriter.println('trait ' + generatedClass.replaceAll('^(.*?)\\.?([^\\.]+)$', '$2') + ' {')
		traitWriter.println('')
		traitWriter.println('\tdef apply(path: String)')
		traitWriter.println('')

		def visited = new HashSet<String>()
		(project.sourceSets.test.resources.srcDirs + project.sourceSets.main.resources.srcDirs).each {

			if (it.exists() && it.isDirectory()) {
				def rootLength = it.absolutePath.length()
				it.eachFileRecurse(FileType.FILES) {
					def path = it.absolutePath.substring(rootLength + 1).replace(java.io.File.separator, '/')

					if (path.matches(testPattern) && visited.add(path)) {

						def name = path.replaceAll('[^\\w\\$]', "_")

						traitWriter.println('\tdef test_' + name + '() {')
						traitWriter.println('\t\tapply("' + path + '")')
						traitWriter.println('\t}')
					}
				}
			}
		}

		traitWriter.println('')
		traitWriter.println('}')
		traitWriter.close()
	}
}
