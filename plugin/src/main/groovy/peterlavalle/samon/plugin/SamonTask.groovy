package peterlavalle.samon.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class SamonTask extends DefaultTask {

	@InputDirectory
	def File templateDir = project.file('src/samon');

	@OutputDirectory
	def File generatedDir = project.file("${project.buildDir}/generated/samon-sources");

	{
		description = 'Translates .samon templates into .scala sources'
		group = 'Source Code Generation'

		project.compileScala.dependsOn(this)
		project.sourceSets.main.scala.srcDirs += generatedDir

		project.dependencies {
			// magically pulls the proper version
			compile SamonVersion.VERSION
		}
	}

	@TaskAction
	def translate() {
		project.fileTree(dir: templateDir, include: ['**/*.samon']).files.sort().each {

			def relative =
					it.absolutePath
							.substring(templateDir.absolutePath.length() + 1, it.absolutePath.length() - 6)
							.replace('\\', '/')

			def output = new File(generatedDir, relative + ".scala")

			assert (output.parentFile.exists() || output.parentFile.mkdirs())

			peterlavalle.samon.FishNChip$.MODULE$.apply(
					relative,
					it.absoluteFile.newInputStream(),
					output.newWriter()
			).close()

			logger.info("[.samon] Template `${relative}` has been witnessed")
		}
	}
}