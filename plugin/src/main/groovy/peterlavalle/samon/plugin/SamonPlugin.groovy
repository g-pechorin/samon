package peterlavalle.samon.plugin

import org.gradle.api.Project
import org.gradle.api.Plugin

class SamonPlugin implements Plugin<Project> {
	void apply(Project project) {
		project.tasks.create(name: 'generateSamon', type: peterlavalle.samon.plugin.SamonTask) {

		}
	}
}